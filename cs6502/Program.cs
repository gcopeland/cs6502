﻿using System;
using System.IO;

namespace cs6502
{
	public class MainClass
	{
		public static void OldMain(string[] args)
		{

			if (args.Length != 1)
			{
				Console.WriteLine("Wrong # of arguments");
				return;
			}

			byte[] buffer = File.ReadAllBytes(args[0]);

			int pc = 0;

			while (pc < buffer.Length)
			{
				++pc;
			}

			Console.WriteLine("Done");
		}



	}


}
