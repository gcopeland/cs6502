﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using cs6502;

namespace m6502
{
    public class Program
    {
        // Page 6 is the area of memory from $600 to $6FF, or in decimal nomenclature, from 1536 to 1791,
        // and ATARI states that none of their software will ever require that space, so it is free for your use. 

        public static int PageSix = 0x600;

        public static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Invalid # of arguments");
                return;
            }

            var bytes = ReadTextFile(args[0]);

            int pc = 0;

            var instructions = new List<Instruction>();

            do
            {
                if (Opcode.Map.ContainsKey(bytes[pc]))
                {
                    var opcode = Opcode.Map[bytes[pc]];
                    var instruction = new Instruction() { Opcode = opcode, Address = PageSix + pc, Values = new List<byte>()};
                    switch (opcode.Length)
                    {
                        case 1:
                            break;
                        case 2:
                            instruction.Values.Add(bytes[pc+1]);
                            break;
                        case 3:
                            // Little-endian
                            instruction.Values.Add(bytes[pc + 2]);
                            instruction.Values.Add(bytes[pc + 1]);
                            break;
                        default:
                            Console.WriteLine("Unexpected Length for opcode " + opcode.Mnemonic + " (" + opcode.Hex + ")");
                            break;
                    }

                    Console.WriteLine(instruction.Text);
                    instructions.Add(instruction);
                    pc += opcode.Length;
                }
                else
                {
                    Console.WriteLine("Unrecogized opcode [" + bytes[pc] + "] at [" + pc + "]");
                    break;
                }

            } while (pc < bytes.Length);

            Processor p = new Processor();

            p.Run(instructions);
            
            Console.ReadKey();
        }

        private static byte[] ReadTextFile(string path)
        {
            var input = File.ReadAllText(path);
            var words = input.Split(' ', '\t', '\n');
            var bytes = new List<byte>();

            foreach (var s in words.Select(w => w.Trim()).Where(w => w.Length > 0))
            {
                try
                {
                    if (s.Length == 2)
                    {
                        byte b = byte.Parse(s, NumberStyles.AllowHexSpecifier);
                        bytes.Add(b);
                    }
                    else if (s.Length == 4)
                    {
                        var s1 = s.Substring(0, 2);
                        var s2 = s.Substring(2, 2);
                        bytes.Add(byte.Parse(s1, NumberStyles.AllowHexSpecifier));
                        bytes.Add(byte.Parse(s2, NumberStyles.AllowHexSpecifier));
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Parse error =>" + s + "<=");
                }
            }

            return bytes.ToArray();
        }
    }
}
