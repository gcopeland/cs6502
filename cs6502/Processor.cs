﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography.X509Certificates;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace cs6502
{
    public delegate int InstructionProc(Instruction instruction);


    public class Processor
    {
        // Accumulator
        public byte A { get; set; }
        // X index
        public byte X { get; set; }
        // Y Index
        public byte Y { get; set; }
        // Stack pointer
        public byte SP { get; set; }
        // Program counter
        public ushort PC { get; set; }

        // Status register
        public bool Negative { get; set; }
        public bool Overflow { get; set; }
        public bool Breakpoint { get; set; }
        public bool Decimal { get; set; }
        public bool Interrupt { get; set; }
        public bool Zero { get; set; }
        public bool Carry { get; set; }

        public byte[] Memory { get; set; }


        public IDictionary<byte, InstructionProc> InstructionProcs;

        public Processor()
        {
            InitInstructions();
            Reset();
        }

        public void Reset()
        {
            A = 0;
            X = 0;
            Y = 0;

            Negative = false;
            Overflow = false;
            Breakpoint = false;
            Decimal = false;
            Interrupt = true;
            Zero = false;
            Carry = false; // Clear before starting addition, Set before starting subtraction 
                            // Addition: 0 = no carry, 1 = carry
                            // Subtraction: 0 = borrow required, 1 = no borrow require

            SP = 0xFF;
            PC = 0x600;
        }

        public void Run(List<Instruction> instructions)
        {
            foreach (var i in instructions)
            {
                RunInstruction(i);
            }

            Console.WriteLine("Run complete");
        }

        public void RunInstruction(Instruction instruction)
        {
//            long startTime = DateTime.Now.Ticks;
//            byte opcode = Fetch();
            int cycles = DecodeExecute(instruction);
//            long endTime = DateTime.Now.Ticks;
//            WasteTime(cycles, endTime - startTime);
        }

        public void WasteTime(int cycles, long interval)
        {
        }

        public int DecodeExecute(Instruction instruction)
        {
            var proc = InstructionProcs[instruction.Opcode.Hex];
            if (proc == null) return -1;
            return proc(instruction);
        }

        public void InitInstructions()
        {
            InstructionProcs = new Dictionary<byte, InstructionProc>()
            {
                { 0x69 , ADC } , { 0x65 , ADC } , { 0x75 , ADC } , { 0x6D , ADC } , { 0x7D , ADC } , { 0x79 , ADC } , { 0x61 , ADC }
                , { 0x71 , ADC } , { 0x29 , AND } , { 0x25 , AND } , { 0x35 , AND } , { 0x2D , AND } , { 0x3D , AND } , { 0x39 , AND }
                , { 0x21 , AND } , { 0x31 , AND } , { 0x0A , ASL } , { 0x06 , ASL } , { 0x16 , ASL } , { 0x0E , ASL } , { 0x1E , ASL }
                , { 0x24 , BIT } , { 0x2C , BIT } , { 0x00 , BRK } , { 0xC9 , CMP } , { 0xC5 , CMP } , { 0xD5 , CMP } , { 0xCD , CMP }
                , { 0xDD , CMP } , { 0xD9 , CMP } , { 0xC1 , CMP } , { 0xD1 , CMP } , { 0xE0 , CPX } , { 0xE4 , CPX } , { 0xEC , CPX }
                , { 0xC0 , CPY } , { 0xC4 , CPY } , { 0xCC , CPY } , { 0xC6 , DEC } , { 0xD6 , DEC } , { 0xCE , DEC } , { 0xDE , DEC }
                , { 0x49 , EOR } , { 0x45 , EOR } , { 0x55 , EOR } , { 0x4D , EOR } , { 0x5D , EOR } , { 0x59 , EOR } , { 0x41 , EOR }
                , { 0x51 , EOR } , { 0xE6 , INC } , { 0xF6 , INC } , { 0xEE , INC } , { 0xFE , INC } , { 0x4C , JMP } , { 0x6C , JMP }
                , { 0x20 , JSR } , { 0xA9 , LDA } , { 0xA5 , LDA } , { 0xB5 , LDA } , { 0xAD , LDA } , { 0xBD , LDA } , { 0xB9 , LDA }
                , { 0xA1 , LDA } , { 0xB1 , LDA } , { 0xA2 , LDX } , { 0xA6 , LDX } , { 0xB6 , LDX } , { 0xAE , LDX } , { 0xBE , LDX }
                , { 0xA0 , LDY } , { 0xA4 , LDY } , { 0xB4 , LDY } , { 0xAC , LDY } , { 0xBC , LDY } , { 0x4A , LSR } , { 0x46 , LSR }
                , { 0x56 , LSR } , { 0x4E , LSR } , { 0x5E , LSR } , { 0xEA , NOP } , { 0x09 , ORA } , { 0x05 , ORA } , { 0x15 , ORA }
                , { 0x0D , ORA } , { 0x1D , ORA } , { 0x19 , ORA } , { 0x01 , ORA } , { 0x11 , ORA } , { 0x2A , ROL } , { 0x26 , ROL }
                , { 0x36 , ROL } , { 0x2E , ROL } , { 0x3E , ROL } , { 0x6A , ROR } , { 0x66 , ROR } , { 0x76 , ROR } , { 0x6E , ROR }
                , { 0x7E , ROR } , { 0x40 , RTI } , { 0x60 , RTS } , { 0xE9 , SBC } , { 0xE5 , SBC } , { 0xF5 , SBC } , { 0xED , SBC }
                , { 0xFD , SBC } , { 0xF9 , SBC } , { 0xE1 , SBC } , { 0xF1 , SBC } , { 0x85 , STA } , { 0x95 , STA } , { 0x8D , STA }
                , { 0x9D , STA } , { 0x99 , STA } , { 0x81 , STA } , { 0x91 , STA } , { 0x86 , STX } , { 0x96 , STX } , { 0x8E , STX }
                , { 0x84 , STY } , { 0x94 , STY } , { 0x8C , STY }
                , { 0x10 , instruction => instruction.Opcode.Cycle } //BPL }
                , { 0x30 , instruction => instruction.Opcode.Cycle } //BMI }
                , { 0x50 , instruction => instruction.Opcode.Cycle } //BVC }
                , { 0x70 , instruction => instruction.Opcode.Cycle } //BVS }
                , { 0x90 , instruction => instruction.Opcode.Cycle } //BCC }
                , { 0xB0 , instruction => instruction.Opcode.Cycle } //BCS }
                , { 0xD0 , instruction => instruction.Opcode.Cycle } //BNE }
                , { 0xF0 , instruction => instruction.Opcode.Cycle } //BEQ }
                , { 0x18 , delegate(Instruction instruction) { Carry = false; return instruction.Opcode.Cycle; }} //CLC }
                , { 0x38 , delegate(Instruction instruction) { Carry = true; return instruction.Opcode.Cycle; }} //SEC }
                , { 0x58 , delegate(Instruction instruction) { Interrupt = false; return instruction.Opcode.Cycle; }} //CLI }
                , { 0x78 , delegate(Instruction instruction) { Interrupt = true; return instruction.Opcode.Cycle; }} //SEI }
                , { 0xB8 , delegate(Instruction instruction) { Overflow = false; return instruction.Opcode.Cycle; }} //CLV }
                , { 0xD8 , delegate(Instruction instruction) { Decimal = false; return instruction.Opcode.Cycle; }} //CLD }
                , { 0xF8 , delegate(Instruction instruction) { Decimal = true; return instruction.Opcode.Cycle; }} //SED }
                , { 0xAA , instruction => instruction.Opcode.Cycle } //TAX }
                , { 0x8A , instruction => instruction.Opcode.Cycle } //TXA }
                , { 0xCA , delegate(Instruction instruction) { X -= 1; return instruction.Opcode.Cycle; }} //DEX }
                , { 0xE8 , delegate(Instruction instruction) { X += 1; return instruction.Opcode.Cycle; }} //INX }
                , { 0xA8 , instruction => instruction.Opcode.Cycle } //TAY }
                , { 0x98 , instruction => instruction.Opcode.Cycle } //TYA }
                , { 0x88 , delegate(Instruction instruction) { Y -= 1; return instruction.Opcode.Cycle; }} //DEY }
                , { 0xC8 , delegate(Instruction instruction) { Y += 1; return instruction.Opcode.Cycle; }} //INY }
                , { 0x9A , instruction => instruction.Opcode.Cycle } //TXS }
                , { 0xBA , instruction => instruction.Opcode.Cycle } //TSX }
                , { 0x48 , instruction => instruction.Opcode.Cycle } //PHA }
                , { 0x68 , instruction => instruction.Opcode.Cycle } //PLA }
                , { 0x08 , instruction => instruction.Opcode.Cycle } //PHP }
                , { 0x28 , instruction => instruction.Opcode.Cycle } //PLP }

            };
        }

        private int NOP(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int CPY(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int JSR(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int JMP(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int INC(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int EOR(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int LSR(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int LDY(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int RTS(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int RTI(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int DEC(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int ROR(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int STA(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int SBC(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int LDX(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int ROL(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int ORA(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int STY(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int STX(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int CPX(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int BRK(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int ASL(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int CMP(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int BIT(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int AND(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int ADC(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }

        private int LDA(Instruction instruction)
        {
            return instruction.Opcode.Cycle;
        }
    }
}
