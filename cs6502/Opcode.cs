﻿using System.Collections.Generic;
using System.Linq;

namespace cs6502
{
    public class Instruction
    {
        public int Address { get; set; }
        public Opcode Opcode { get; set; }
        public List<byte> Values { get; set; }
        public string Text {
            get
            {
                var values = "";
                values = (Values.Count == 0) ? "" : Values.Aggregate(values, (current, v) => current + v.ToString("X2"));
                return Address.ToString("X2")
                       + ": "
                       + Opcode.Mnemonic
                       + " "
                       + values
                       + "\t"
                       + Opcode.ModeToString[Opcode.Mode];
            }
        }
    }

    public class Opcode
    {
        public string Mnemonic { get; set; }
        public byte Hex { get; set; }
        public byte Length { get; set; }
        public byte Cycle { get; set; }
        public int Mode { get; set; }

        public static readonly IDictionary<byte, Opcode> Map = new Dictionary<byte, Opcode>()
        {
              { 0x69 , new Opcode() {Mnemonic = "ADC", Hex = 0x69, Length = 2, Cycle = 2, Mode = 1}}
            , { 0x65 , new Opcode() {Mnemonic = "ADC", Hex = 0x65, Length = 2, Cycle = 3, Mode = 2}}
            , { 0x75 , new Opcode() {Mnemonic = "ADC", Hex = 0x75, Length = 2, Cycle = 4, Mode = 3}}
            , { 0x6D , new Opcode() {Mnemonic = "ADC", Hex = 0x6D, Length = 3, Cycle = 4, Mode = 5}}
            , { 0x7D , new Opcode() {Mnemonic = "ADC", Hex = 0x7D, Length = 3, Cycle = 4, Mode = 6}}
            , { 0x79 , new Opcode() {Mnemonic = "ADC", Hex = 0x79, Length = 3, Cycle = 4, Mode = 7}}
            , { 0x61 , new Opcode() {Mnemonic = "ADC", Hex = 0x61, Length = 2, Cycle = 6, Mode = 8}}
            , { 0x71 , new Opcode() {Mnemonic = "ADC", Hex = 0x71, Length = 2, Cycle = 5, Mode = 9}}
            , { 0x29 , new Opcode() {Mnemonic = "AND", Hex = 0x29, Length = 2, Cycle = 2, Mode = 1}}
            , { 0x25 , new Opcode() {Mnemonic = "AND", Hex = 0x25, Length = 2, Cycle = 3, Mode = 2}}
            , { 0x35 , new Opcode() {Mnemonic = "AND", Hex = 0x35, Length = 2, Cycle = 4, Mode = 3}}
            , { 0x2D , new Opcode() {Mnemonic = "AND", Hex = 0x2D, Length = 3, Cycle = 4, Mode = 5}}
            , { 0x3D , new Opcode() {Mnemonic = "AND", Hex = 0x3D, Length = 3, Cycle = 4, Mode = 6}}
            , { 0x39 , new Opcode() {Mnemonic = "AND", Hex = 0x39, Length = 3, Cycle = 4, Mode = 7}}
            , { 0x21 , new Opcode() {Mnemonic = "AND", Hex = 0x21, Length = 2, Cycle = 6, Mode = 8}}
            , { 0x31 , new Opcode() {Mnemonic = "AND", Hex = 0x31, Length = 2, Cycle = 5, Mode = 9}}
            , { 0x0A , new Opcode() {Mnemonic = "ASL", Hex = 0x0A, Length = 1, Cycle = 2, Mode = 11}}
            , { 0x06 , new Opcode() {Mnemonic = "ASL", Hex = 0x06, Length = 2, Cycle = 5, Mode = 2}}
            , { 0x16 , new Opcode() {Mnemonic = "ASL", Hex = 0x16, Length = 2, Cycle = 6, Mode = 3}}
            , { 0x0E , new Opcode() {Mnemonic = "ASL", Hex = 0x0E, Length = 3, Cycle = 6, Mode = 5}}
            , { 0x1E , new Opcode() {Mnemonic = "ASL", Hex = 0x1E, Length = 3, Cycle = 7, Mode = 6}}
            , { 0x24 , new Opcode() {Mnemonic = "BIT", Hex = 0x24, Length = 2, Cycle = 3, Mode = 2}}
            , { 0x2C , new Opcode() {Mnemonic = "BIT", Hex = 0x2C, Length = 3, Cycle = 4, Mode = 5}}
            , { 0x00 , new Opcode() {Mnemonic = "BRK", Hex = 0x00, Length = 1, Cycle = 7, Mode = 13}}
            , { 0xC9 , new Opcode() {Mnemonic = "CMP", Hex = 0xC9, Length = 2, Cycle = 2, Mode = 1}}
            , { 0xC5 , new Opcode() {Mnemonic = "CMP", Hex = 0xC5, Length = 2, Cycle = 3, Mode = 2}}
            , { 0xD5 , new Opcode() {Mnemonic = "CMP", Hex = 0xD5, Length = 2, Cycle = 4, Mode = 3}}
            , { 0xCD , new Opcode() {Mnemonic = "CMP", Hex = 0xCD, Length = 3, Cycle = 4, Mode = 5}}
            , { 0xDD , new Opcode() {Mnemonic = "CMP", Hex = 0xDD, Length = 3, Cycle = 4, Mode = 6}}
            , { 0xD9 , new Opcode() {Mnemonic = "CMP", Hex = 0xD9, Length = 3, Cycle = 4, Mode = 7}}
            , { 0xC1 , new Opcode() {Mnemonic = "CMP", Hex = 0xC1, Length = 2, Cycle = 6, Mode = 8}}
            , { 0xD1 , new Opcode() {Mnemonic = "CMP", Hex = 0xD1, Length = 2, Cycle = 5, Mode = 9}}
            , { 0xE0 , new Opcode() {Mnemonic = "CPX", Hex = 0xE0, Length = 2, Cycle = 2, Mode = 1}}
            , { 0xE4 , new Opcode() {Mnemonic = "CPX", Hex = 0xE4, Length = 2, Cycle = 3, Mode = 2}}
            , { 0xEC , new Opcode() {Mnemonic = "CPX", Hex = 0xEC, Length = 3, Cycle = 4, Mode = 5}}
            , { 0xC0 , new Opcode() {Mnemonic = "CPY", Hex = 0xC0, Length = 2, Cycle = 2, Mode = 1}}
            , { 0xC4 , new Opcode() {Mnemonic = "CPY", Hex = 0xC4, Length = 2, Cycle = 3, Mode = 2}}
            , { 0xCC , new Opcode() {Mnemonic = "CPY", Hex = 0xCC, Length = 3, Cycle = 4, Mode = 5}}
            , { 0xC6 , new Opcode() {Mnemonic = "DEC", Hex = 0xC6, Length = 2, Cycle = 5, Mode = 2}}
            , { 0xD6 , new Opcode() {Mnemonic = "DEC", Hex = 0xD6, Length = 2, Cycle = 6, Mode = 3}}
            , { 0xCE , new Opcode() {Mnemonic = "DEC", Hex = 0xCE, Length = 3, Cycle = 6, Mode = 5}}
            , { 0xDE , new Opcode() {Mnemonic = "DEC", Hex = 0xDE, Length = 3, Cycle = 7, Mode = 6}}
            , { 0x49 , new Opcode() {Mnemonic = "EOR", Hex = 0x49, Length = 2, Cycle = 2, Mode = 1}}
            , { 0x45 , new Opcode() {Mnemonic = "EOR", Hex = 0x45, Length = 2, Cycle = 3, Mode = 2}}
            , { 0x55 , new Opcode() {Mnemonic = "EOR", Hex = 0x55, Length = 2, Cycle = 4, Mode = 3}}
            , { 0x4D , new Opcode() {Mnemonic = "EOR", Hex = 0x4D, Length = 3, Cycle = 4, Mode = 5}}
            , { 0x5D , new Opcode() {Mnemonic = "EOR", Hex = 0x5D, Length = 3, Cycle = 4, Mode = 6}}
            , { 0x59 , new Opcode() {Mnemonic = "EOR", Hex = 0x59, Length = 3, Cycle = 4, Mode = 7}}
            , { 0x41 , new Opcode() {Mnemonic = "EOR", Hex = 0x41, Length = 2, Cycle = 6, Mode = 8}}
            , { 0x51 , new Opcode() {Mnemonic = "EOR", Hex = 0x51, Length = 2, Cycle = 5, Mode = 9}}
            , { 0xE6 , new Opcode() {Mnemonic = "INC", Hex = 0xE6, Length = 2, Cycle = 5, Mode = 2}}
            , { 0xF6 , new Opcode() {Mnemonic = "INC", Hex = 0xF6, Length = 2, Cycle = 6, Mode = 3}}
            , { 0xEE , new Opcode() {Mnemonic = "INC", Hex = 0xEE, Length = 3, Cycle = 6, Mode = 5}}
            , { 0xFE , new Opcode() {Mnemonic = "INC", Hex = 0xFE, Length = 3, Cycle = 7, Mode = 6}}
            , { 0x4C , new Opcode() {Mnemonic = "JMP", Hex = 0x4C, Length = 3, Cycle = 3, Mode = 5}}
            , { 0x6C , new Opcode() {Mnemonic = "JMP", Hex = 0x6C, Length = 3, Cycle = 5, Mode = 12}}
            , { 0x20 , new Opcode() {Mnemonic = "JSR", Hex = 0x20, Length = 3, Cycle = 6, Mode = 5}}
            , { 0xA9 , new Opcode() {Mnemonic = "LDA", Hex = 0xA9, Length = 2, Cycle = 2, Mode = 1}}
            , { 0xA5 , new Opcode() {Mnemonic = "LDA", Hex = 0xA5, Length = 2, Cycle = 3, Mode = 2}}
            , { 0xB5 , new Opcode() {Mnemonic = "LDA", Hex = 0xB5, Length = 2, Cycle = 4, Mode = 3}}
            , { 0xAD , new Opcode() {Mnemonic = "LDA", Hex = 0xAD, Length = 3, Cycle = 4, Mode = 5}}
            , { 0xBD , new Opcode() {Mnemonic = "LDA", Hex = 0xBD, Length = 3, Cycle = 4, Mode = 6}}
            , { 0xB9 , new Opcode() {Mnemonic = "LDA", Hex = 0xB9, Length = 3, Cycle = 4, Mode = 7}}
            , { 0xA1 , new Opcode() {Mnemonic = "LDA", Hex = 0xA1, Length = 2, Cycle = 6, Mode = 8}}
            , { 0xB1 , new Opcode() {Mnemonic = "LDA", Hex = 0xB1, Length = 2, Cycle = 5, Mode = 9}}
            , { 0xA2 , new Opcode() {Mnemonic = "LDX", Hex = 0xA2, Length = 2, Cycle = 2, Mode = 1}}
            , { 0xA6 , new Opcode() {Mnemonic = "LDX", Hex = 0xA6, Length = 2, Cycle = 3, Mode = 2}}
            , { 0xB6 , new Opcode() {Mnemonic = "LDX", Hex = 0xB6, Length = 2, Cycle = 4, Mode = 4}}
            , { 0xAE , new Opcode() {Mnemonic = "LDX", Hex = 0xAE, Length = 3, Cycle = 4, Mode = 5}}
            , { 0xBE , new Opcode() {Mnemonic = "LDX", Hex = 0xBE, Length = 3, Cycle = 4, Mode = 7}}
            , { 0xA0 , new Opcode() {Mnemonic = "LDY", Hex = 0xA0, Length = 2, Cycle = 2, Mode = 1}}
            , { 0xA4 , new Opcode() {Mnemonic = "LDY", Hex = 0xA4, Length = 2, Cycle = 3, Mode = 2}}
            , { 0xB4 , new Opcode() {Mnemonic = "LDY", Hex = 0xB4, Length = 2, Cycle = 4, Mode = 3}}
            , { 0xAC , new Opcode() {Mnemonic = "LDY", Hex = 0xAC, Length = 3, Cycle = 4, Mode = 5}}
            , { 0xBC , new Opcode() {Mnemonic = "LDY", Hex = 0xBC, Length = 3, Cycle = 4, Mode = 6}}
            , { 0x4A , new Opcode() {Mnemonic = "LSR", Hex = 0x4A, Length = 1, Cycle = 2, Mode = 11}}
            , { 0x46 , new Opcode() {Mnemonic = "LSR", Hex = 0x46, Length = 2, Cycle = 5, Mode = 2}}
            , { 0x56 , new Opcode() {Mnemonic = "LSR", Hex = 0x56, Length = 2, Cycle = 6, Mode = 3}}
            , { 0x4E , new Opcode() {Mnemonic = "LSR", Hex = 0x4E, Length = 3, Cycle = 6, Mode = 5}}
            , { 0x5E , new Opcode() {Mnemonic = "LSR", Hex = 0x5E, Length = 3, Cycle = 7, Mode = 6}}
            , { 0xEA , new Opcode() {Mnemonic = "NOP", Hex = 0xEA, Length = 1, Cycle = 2, Mode = 13}}
            , { 0x09 , new Opcode() {Mnemonic = "ORA", Hex = 0x09, Length = 2, Cycle = 2, Mode = 1}}
            , { 0x05 , new Opcode() {Mnemonic = "ORA", Hex = 0x05, Length = 2, Cycle = 3, Mode = 2}}
            , { 0x15 , new Opcode() {Mnemonic = "ORA", Hex = 0x15, Length = 2, Cycle = 4, Mode = 3}}
            , { 0x0D , new Opcode() {Mnemonic = "ORA", Hex = 0x0D, Length = 3, Cycle = 4, Mode = 5}}
            , { 0x1D , new Opcode() {Mnemonic = "ORA", Hex = 0x1D, Length = 3, Cycle = 4, Mode = 6}}
            , { 0x19 , new Opcode() {Mnemonic = "ORA", Hex = 0x19, Length = 3, Cycle = 4, Mode = 7}}
            , { 0x01 , new Opcode() {Mnemonic = "ORA", Hex = 0x01, Length = 2, Cycle = 6, Mode = 8}}
            , { 0x11 , new Opcode() {Mnemonic = "ORA", Hex = 0x11, Length = 2, Cycle = 5, Mode = 9}}
            , { 0x2A , new Opcode() {Mnemonic = "ROL", Hex = 0x2A, Length = 1, Cycle = 2, Mode = 11}}
            , { 0x26 , new Opcode() {Mnemonic = "ROL", Hex = 0x26, Length = 2, Cycle = 5, Mode = 2}}
            , { 0x36 , new Opcode() {Mnemonic = "ROL", Hex = 0x36, Length = 2, Cycle = 6, Mode = 3}}
            , { 0x2E , new Opcode() {Mnemonic = "ROL", Hex = 0x2E, Length = 3, Cycle = 6, Mode = 5}}
            , { 0x3E , new Opcode() {Mnemonic = "ROL", Hex = 0x3E, Length = 3, Cycle = 7, Mode = 6}}
            , { 0x6A , new Opcode() {Mnemonic = "ROR", Hex = 0x6A, Length = 1, Cycle = 2, Mode = 11}}
            , { 0x66 , new Opcode() {Mnemonic = "ROR", Hex = 0x66, Length = 2, Cycle = 5, Mode = 2}}
            , { 0x76 , new Opcode() {Mnemonic = "ROR", Hex = 0x76, Length = 2, Cycle = 6, Mode = 3}}
            , { 0x6E , new Opcode() {Mnemonic = "ROR", Hex = 0x6E, Length = 3, Cycle = 6, Mode = 5}}
            , { 0x7E , new Opcode() {Mnemonic = "ROR", Hex = 0x7E, Length = 3, Cycle = 7, Mode = 6}}
            , { 0x40 , new Opcode() {Mnemonic = "RTI", Hex = 0x40, Length = 1, Cycle = 6, Mode = 13}}
            , { 0x60 , new Opcode() {Mnemonic = "RTS", Hex = 0x60, Length = 1, Cycle = 6, Mode = 13}}
            , { 0xE9 , new Opcode() {Mnemonic = "SBC", Hex = 0xE9, Length = 2, Cycle = 2, Mode = 1}}
            , { 0xE5 , new Opcode() {Mnemonic = "SBC", Hex = 0xE5, Length = 2, Cycle = 3, Mode = 2}}
            , { 0xF5 , new Opcode() {Mnemonic = "SBC", Hex = 0xF5, Length = 2, Cycle = 4, Mode = 3}}
            , { 0xED , new Opcode() {Mnemonic = "SBC", Hex = 0xED, Length = 3, Cycle = 4, Mode = 5}}
            , { 0xFD , new Opcode() {Mnemonic = "SBC", Hex = 0xFD, Length = 3, Cycle = 4, Mode = 6}}
            , { 0xF9 , new Opcode() {Mnemonic = "SBC", Hex = 0xF9, Length = 3, Cycle = 4, Mode = 7}}
            , { 0xE1 , new Opcode() {Mnemonic = "SBC", Hex = 0xE1, Length = 2, Cycle = 6, Mode = 8}}
            , { 0xF1 , new Opcode() {Mnemonic = "SBC", Hex = 0xF1, Length = 2, Cycle = 5, Mode = 9}}
            , { 0x85 , new Opcode() {Mnemonic = "STA", Hex = 0x85, Length = 2, Cycle = 3, Mode = 2}}
            , { 0x95 , new Opcode() {Mnemonic = "STA", Hex = 0x95, Length = 2, Cycle = 4, Mode = 3}}
            , { 0x8D , new Opcode() {Mnemonic = "STA", Hex = 0x8D, Length = 3, Cycle = 4, Mode = 5}}
            , { 0x9D , new Opcode() {Mnemonic = "STA", Hex = 0x9D, Length = 3, Cycle = 5, Mode = 6}}
            , { 0x99 , new Opcode() {Mnemonic = "STA", Hex = 0x99, Length = 3, Cycle = 5, Mode = 7}}
            , { 0x81 , new Opcode() {Mnemonic = "STA", Hex = 0x81, Length = 2, Cycle = 6, Mode = 8}}
            , { 0x91 , new Opcode() {Mnemonic = "STA", Hex = 0x91, Length = 2, Cycle = 6, Mode = 9}}
            , { 0x86 , new Opcode() {Mnemonic = "STX", Hex = 0x86, Length = 2, Cycle = 3, Mode = 2}}
            , { 0x96 , new Opcode() {Mnemonic = "STX", Hex = 0x96, Length = 2, Cycle = 4, Mode = 4}}
            , { 0x8E , new Opcode() {Mnemonic = "STX", Hex = 0x8E, Length = 3, Cycle = 4, Mode = 5}}
            , { 0x84 , new Opcode() {Mnemonic = "STY", Hex = 0x84, Length = 2, Cycle = 3, Mode = 2}}
            , { 0x94 , new Opcode() {Mnemonic = "STY", Hex = 0x94, Length = 2, Cycle = 4, Mode = 3}}
            , { 0x8C , new Opcode() {Mnemonic = "STY", Hex = 0x8C, Length = 3, Cycle = 4, Mode = 5}}
            , { 0x10 , new Opcode() {Mnemonic = "BPL", Hex = 0x10, Length = 2, Cycle = 2, Mode = 10}}
            , { 0x30 , new Opcode() {Mnemonic = "BMI", Hex = 0x30, Length = 2, Cycle = 2, Mode = 10}}
            , { 0x50 , new Opcode() {Mnemonic = "BVC", Hex = 0x50, Length = 2, Cycle = 2, Mode = 10}}
            , { 0x70 , new Opcode() {Mnemonic = "BVS", Hex = 0x70, Length = 2, Cycle = 2, Mode = 10}}
            , { 0x90 , new Opcode() {Mnemonic = "BCC", Hex = 0x90, Length = 2, Cycle = 2, Mode = 10}}
            , { 0xB0 , new Opcode() {Mnemonic = "BCS", Hex = 0xB0, Length = 2, Cycle = 2, Mode = 10}}
            , { 0xD0 , new Opcode() {Mnemonic = "BNE", Hex = 0xD0, Length = 2, Cycle = 2, Mode = 10}}
            , { 0xF0 , new Opcode() {Mnemonic = "BEQ", Hex = 0xF0, Length = 2, Cycle = 2, Mode = 10}}
            , { 0x18 , new Opcode() {Mnemonic = "CLC", Hex = 0x18, Length = 1, Cycle = 2, Mode = 13}}
            , { 0x38 , new Opcode() {Mnemonic = "SEC", Hex = 0x38, Length = 1, Cycle = 2, Mode = 13}}
            , { 0x58 , new Opcode() {Mnemonic = "CLI", Hex = 0x58, Length = 1, Cycle = 2, Mode = 13}}
            , { 0x78 , new Opcode() {Mnemonic = "SEI", Hex = 0x78, Length = 1, Cycle = 2, Mode = 13}}
            , { 0xB8 , new Opcode() {Mnemonic = "CLV", Hex = 0xB8, Length = 1, Cycle = 2, Mode = 13}}
            , { 0xD8 , new Opcode() {Mnemonic = "CLD", Hex = 0xD8, Length = 1, Cycle = 2, Mode = 13}}
            , { 0xF8 , new Opcode() {Mnemonic = "SED", Hex = 0xF8, Length = 1, Cycle = 2, Mode = 13}}
            , { 0xAA , new Opcode() {Mnemonic = "TAX", Hex = 0xAA, Length = 1, Cycle = 2, Mode = 13}}
            , { 0x8A , new Opcode() {Mnemonic = "TXA", Hex = 0x8A, Length = 1, Cycle = 2, Mode = 13}}
            , { 0xCA , new Opcode() {Mnemonic = "DEX", Hex = 0xCA, Length = 1, Cycle = 2, Mode = 13}}
            , { 0xE8 , new Opcode() {Mnemonic = "INX", Hex = 0xE8, Length = 1, Cycle = 2, Mode = 13}}
            , { 0xA8 , new Opcode() {Mnemonic = "TAY", Hex = 0xA8, Length = 1, Cycle = 2, Mode = 13}}
            , { 0x98 , new Opcode() {Mnemonic = "TYA", Hex = 0x98, Length = 1, Cycle = 2, Mode = 13}}
            , { 0x88 , new Opcode() {Mnemonic = "DEY", Hex = 0x88, Length = 1, Cycle = 2, Mode = 13}}
            , { 0xC8 , new Opcode() {Mnemonic = "INY", Hex = 0xC8, Length = 1, Cycle = 2, Mode = 13}}
            , { 0x9A , new Opcode() {Mnemonic = "TXS", Hex = 0x9A, Length = 1, Cycle = 2, Mode = 13}}
            , { 0xBA , new Opcode() {Mnemonic = "TSX", Hex = 0xBA, Length = 1, Cycle = 2, Mode = 13}}
            , { 0x48 , new Opcode() {Mnemonic = "PHA", Hex = 0x48, Length = 1, Cycle = 3, Mode = 13}}
            , { 0x68 , new Opcode() {Mnemonic = "PLA", Hex = 0x68, Length = 1, Cycle = 4, Mode = 13}}
            , { 0x08 , new Opcode() {Mnemonic = "PHP", Hex = 0x08, Length = 1, Cycle = 3, Mode = 13}}
            , { 0x28 , new Opcode() {Mnemonic = "PLP", Hex = 0x28, Length = 1, Cycle = 4, Mode = 13}}
        };

        public static readonly IDictionary<string, int> StringToMode = new Dictionary<string, int>()
        {
            { "Immediate", 1 }, { "Zero Page", 2 }, { "Zero Page,X", 3 }, { "Zero Page,Y", 4 },
            { "Absolute", 5 }, { "Absolute,X", 6 }, { "Absolute,Y", 7 },
            { "Indirect,X", 8 }, { "Indirect,Y", 9 },
            { "Relative", 10 }, { "Accumulator", 11 }, { "Indirect", 12 }, { "Implied", 13 }};

        public static readonly IDictionary<int, string> ModeToString = new Dictionary<int, string>()
        {
            { 1, "Immediate" }, { 2, "Zero Page" }, { 3, "Zero Page,X" }, { 4, "Zero Page,Y" },
            { 5, "Absolute" }, { 6, "Absolute,X" }, { 7, "Absolute,Y" },
            { 8, "Indirect,X" }, { 9, "Indirect,Y" },
            { 10, "Relative" }, { 11, "Accumulator" }, { 12, "Indirect" }, { 13, "Implied" }
        };

    }
}
